from abc import ABC, abstractmethod


# Subscription classes
class Subscription(ABC):
    @abstractmethod
    def get_monthly_fee(self):
        pass

    @abstractmethod
    def get_minimum_period(self):
        pass

    @abstractmethod
    def get_channels(self):
        pass


class DomesticSubscription(Subscription):
    def get_monthly_fee(self):
        return 10

    def get_minimum_period(self):
        return 1

    def get_channels(self):
        return ["News", "Entertainment", "Sports"]


class EducationalSubscription(Subscription):
    def get_monthly_fee(self):
        return 5

    def get_minimum_period(self):
        return 6

    def get_channels(self):
        return ["Documentaries", "Science", "History"]


class PremiumSubscription(Subscription):
    def get_monthly_fee(self):
        return 20

    def get_minimum_period(self):
        return 1

    def get_channels(self):
        return ["Movies", "Sports", "Premium Series", "News"]


# Creator classes
class SubscriptionCreator(ABC):
    @abstractmethod
    def create_subscription(self, type):
        pass


class WebSite(SubscriptionCreator):
    def create_subscription(self, type):
        if type == "Domestic":
            return DomesticSubscription()
        elif type == "Educational":
            return EducationalSubscription()
        elif type == "Premium":
            return PremiumSubscription()


class MobileApp(SubscriptionCreator):
    def create_subscription(self, type):
        if type == "Domestic":
            return DomesticSubscription()
        elif type == "Educational":
            return EducationalSubscription()
        elif type == "Premium":
            return PremiumSubscription()


class ManagerCall(SubscriptionCreator):
    def create_subscription(self, type):
        if type == "Domestic":
            return DomesticSubscription()
        elif type == "Educational":
            return EducationalSubscription()
        elif type == "Premium":
            return PremiumSubscription()


# Testing
def main():
    creators = [WebSite(), MobileApp(), ManagerCall()]
    types = ["Domestic", "Educational", "Premium"]

    for creator in creators:
        for type in types:
            subscription = creator.create_subscription(type)
            print(f"Created {type} subscription via {creator.__class__.__name__}:")
            print(f"Monthly Fee: ${subscription.get_monthly_fee()}")
            print(f"Minimum Period: {subscription.get_minimum_period()} months")
            print(f"Channels: {', '.join(subscription.get_channels())}")
            print("")


if __name__ == "__main__":
    main()