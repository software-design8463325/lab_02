import copy

class Virus:
    def __init__(self, name, weight, age, species):
        self.name = name
        self.weight = weight
        self.age = age
        self.species = species
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def clone(self):
        return copy.deepcopy(self)

# Testing
def main():
    parent = Virus("ParentVirus", 1.0, 5, "TypeA")
    child1 = Virus("ChildVirus1", 0.5, 2, "TypeA")
    child2 = Virus("ChildVirus2", 0.4, 1, "TypeA")
    parent.add_child(child1)
    parent.add_child(child2)

    cloned_parent = parent.clone()

    print(f"Original Parent: {parent.name}, Children: {[child.name for child in parent.children]}")
    print(f"Cloned Parent: {cloned_parent.name}, Children: {[child.name for child in cloned_parent.children]}")

if __name__ == "__main__":
    main()