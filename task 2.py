from abc import ABC, abstractmethod


# Device classes
class Device(ABC):
    @abstractmethod
    def get_type(self):
        pass


class Laptop(Device):
    def get_type(self):
        return "Laptop"


class Netbook(Device):
    def get_type(self):
        return "Netbook"


class EBook(Device):
    def get_type(self):
        return "EBook"


class Smartphone(Device):
    def get_type(self):
        return "Smartphone"


# Brand-specific factories
class DeviceFactory(ABC):
    @abstractmethod
    def create_laptop(self):
        pass

    @abstractmethod
    def create_netbook(self):
        pass

    @abstractmethod
    def create_ebook(self):
        pass

    @abstractmethod
    def create_smartphone(self):
        pass


class IProneFactory(DeviceFactory):
    def create_laptop(self):
        return Laptop()

    def create_netbook(self):
        return Netbook()

    def create_ebook(self):
        return EBook()

    def create_smartphone(self):
        return Smartphone()


class KiaomiFactory(DeviceFactory):
    def create_laptop(self):
        return Laptop()

    def create_netbook(self):
        return Netbook()

    def create_ebook(self):
        return EBook()

    def create_smartphone(self):
        return Smartphone()


class BalaxyFactory(DeviceFactory):
    def create_laptop(self):
        return Laptop()

    def create_netbook(self):
        return Netbook()

    def create_ebook(self):
        return EBook()

    def create_smartphone(self):
        return Smartphone()


# Testing
def main():
    factories = [IProneFactory(), KiaomiFactory(), BalaxyFactory()]
    device_types = ["Laptop", "Netbook", "EBook", "Smartphone"]

    for factory in factories:
        for device_type in device_types:
            device = getattr(factory, f'create_{device_type.lower()}')()
            print(f"Created {device.get_type()} from {factory.__class__.__name__}")


if __name__ == "__main__":
    main()