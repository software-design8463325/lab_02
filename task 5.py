class Character:
    def __init__(self):
        self.attributes = {}

    def __str__(self):
        return str(self.attributes)

class CharacterBuilder:
    def __init__(self):
        self.character = Character()

    def set_height(self, height):
        self.character.attributes["height"] = height
        return self

    def set_build(self, build):
        self.character.attributes["build"] = build
        return self

    def set_hair_color(self, hair_color):
        self.character.attributes["hair_color"] = hair_color
        return self

    def set_eye_color(self, eye_color):
        self.character.attributes["eye_color"] = eye_color
        return self

    def set_clothing(self, clothing):
        self.character.attributes["clothing"] = clothing
        return self

    def set_inventory(self, inventory):
        self.character.attributes["inventory"] = inventory
        return self

    def build(self):
        return self.character

class HeroBuilder(CharacterBuilder):
    def set_hero_name(self, name):
        self.character.attributes["hero_name"] = name
        return self

class EnemyBuilder(CharacterBuilder):
    def set_enemy_name(self, name):
        self.character.attributes["enemy_name"] = name
        return self

# Director
class Director:
    def __init__(self, builder):
        self.builder = builder

    def construct(self):
        return (self.builder
                .set_height("6 feet")
                .set_build("Muscular")
                .set_hair_color("Black")
                .set_eye_color("Blue")
                .set_clothing("Armor")
                .set_inventory(["Sword", "Shield"])
                .build())

# Testing
def main():
    hero_builder = HeroBuilder().set_hero_name("Archer")
    enemy_builder = EnemyBuilder().set_enemy_name("Goblin King")

    director = Director(hero_builder)
    hero = director.construct()
    print(f"Hero: {hero}")

    director = Director(enemy_builder)
    enemy = director.construct()
    print(f"Enemy: {enemy}")

if __name__ == "__main__":
    main()